import customers from './data/customers.json'
import products from './data/products.json'
import receipts from './data/receipts.json'
import { ByDay, Customer, CustomerReport, Item, ProductsObject } from './models/models';

const formatDate = (dateString: string): string => {
	const date = new Date(dateString);
	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getDate();

	return `${year}-${month < 10 ? `0${month}` : month}-${day < 10 ? `0${day}` : day}`;
}

export function generateCustomerReport(customerId: string): CustomerReport {
	const customer = customers.find(customer => customer.id === customerId) || {} as Customer;

	const productsObject = products.reduce((acc, product) => ({
		...acc,
		[product.id]: {
			...product
		}
	}), {} as ProductsObject);

	const itemsUnsorted = receipts.reduce((acc, receipt) => {
		if (receipt.customerId !== customerId) {
			return acc;
		}

		return [
			...acc,
			{
				...receipt,
				...productsObject[receipt.productId],
				price: productsObject[receipt.productId]?.price ? parseFloat(productsObject[receipt.productId].price) : 0,
			}
		]
	}, [] as Item[]);

	const items = itemsUnsorted.sort((a, b) => {
		const dateA = new Date(a.createdAt).getTime();
		const dateB = new Date(b.createdAt).getTime();

		return dateA - dateB;
	});

	const totals = items.reduce((acc, item) => {
		const date = formatDate(item.createdAt);
		const totalItem = item.price * item.quantity;

		return {
			total: acc.total + totalItem,
			byDay: {
				...acc.byDay,
				[date]: date in acc.byDay ? acc.byDay[date] + totalItem : totalItem,
			}
		}
	}, { total: 0, byDay: {} as ByDay})

	return {
		customer,
		items,
		...totals,
	}
}
